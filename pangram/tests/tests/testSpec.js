/**
 * test template
 */
describe("scenario",function(){

  var pangramIn;
  beforeEach(function(){
    pangramIn = pangram();
  });
  afterEach(function(){
  });

  it("check phrase should return 1",function(){
    var frase = 'abcdefghilmnopqrstuvzyxwkj';
    expect(pangramIn.checkPhrase(frase)).toBe(1);
  });

  it("check phrase should return 0",function(){
    var frase = 'abcdefghilmnopqrstuvzywkj';
    expect(pangramIn.checkPhrase(frase)).toBe(0);
  });

  it("check phrase should return 1 with example #1",function(){
    var frase = 'We promptly judged antique ivory buckles for the next prize';
    expect(pangramIn.checkPhrase(frase)).toBe(1);
  });

  it("check phrase should return 0 with example #2",function(){
    var frase = 'We promptly judged antique ivory buckles for the prize';
    expect(pangramIn.checkPhrase(frase)).toBe(0);
  });
});
