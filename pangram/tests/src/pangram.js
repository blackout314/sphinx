

var pangram = function() {
  return {
    hello: function() {
      return 'world';
    },
    checkPhrase: function(phrase) {
      var alphabet = 'abcdefghilmnopqrstuvzyxwkj';
      var clearPhrase = phrase.toLowerCase();
      for(var i = 0; i < alphabet.length; i++) {
        var char = alphabet[i];
        if(clearPhrase.indexOf(char) === -1) {
          return 0;
        }
      }
      return 1;
    }
  }
};
